# FFAdvantasy

Pixel font mod for FF Pixel Remaster series (PC), based on the table in "Final Fantasy I*II Advance" for Game Boy Advance. 

Use the font bundle files in the /StreamingAssets folder with those in the game's (ex. /steamapps/common/FINAL FANTASY PR/FINAL FANTASY_Data/StreamingAssets/).

TrueType/OpenType font files are found in the /sourcefont folder.